/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:03:41 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/26 01:30:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	int		i;

	i = 0;
	if (!n)
		return (dest);
	while (dest[i])
		i++;
	while (*src && n)
	{
		dest[i++] = *(src++);
		n--;
	}
	dest[i] = '\0';
	return (dest);
}
