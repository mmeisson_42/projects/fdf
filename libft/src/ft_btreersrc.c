/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreersrc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:54:09 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:04:51 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_btree		*ft_btreersrc(const t_btree *nbtree, const void *data,
		size_t data_size, int (*cmp)(const void*, const void*, size_t))
{
	int		i;

	if (!nbtree)
		return (NULL);
	i = cmp(nbtree->content, data, data_size);
	if (!i && data_size == nbtree->content_size)
		return ((t_btree*)nbtree);
	else if (i < 0 || (!i && data_size < nbtree->content_size))
		return (ft_btreersrc(nbtree->left, data, data_size, cmp));
	else
		return (ft_btreersrc(nbtree->right, data, data_size, cmp));
}
