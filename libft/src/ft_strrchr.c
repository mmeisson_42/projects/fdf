/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:04:18 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/27 15:33:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	cbis;
	int		i;

	cbis = (char)c;
	i = ft_strlen(s);
	while (i >= 0)
	{
		if (s[i] == cbis)
			return ((char*)&(s[i]));
		i--;
	}
	return (NULL);
}
