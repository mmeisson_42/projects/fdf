# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/03/15 21:10:45 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = fdf

SRC_PATH = ./src/
SRC_NAME = fdf.c ft_get_map.c get_next_line.c draw_line.c ft_proj.c\
		   ft_cal_coord.c init_e.c hook.c color.c ft_error.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./includes/ 

LIB_PATH = ./libft/

CFLAGS = -Wall -Werror -Wextra

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_PATH))
INC = $(addprefix -I,$(INC_PATH))

LIB_NAMES = -lft -lmlx
LDFLAGS = $(LIB) $(LIB_NAMES) -framework OpenGL -framework AppKit


all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIB_PATH)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH) 2> /dev/null
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	@rm -f $(OBJ)
	@rmdir $(OBJ_PATH) 2>/dev/null || echo "" > /dev/null

fclean: clean
	@rm -f $(NAME)

fcleanall: clean
	make -C $(LIB_PATH) fclean
	@rm -f $(NAME)

re: fclean all
