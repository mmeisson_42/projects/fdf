/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:23:46 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/24 13:45:50 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# define WIN_W 800
# define WIN_H 500
# define COLOR PINK

# include <errno.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdio.h>
# include "mlx.h"
# include "get_next_line.h"
# include "libft.h"

typedef enum		e_moussecode
{
	LEFT = 1,
	RIGHT = 2,
	MIDDLE = 3,
	W_TOP = 4,
	W_BOT = 5,
	W_RIGHT = 6,
	W_LEFT = 7,
}					t_moussecode;

typedef enum		e_keycode
{
	ESC = 53,
	ENTER = 36,
	SPACE = 49,
	DEL = 117,
	TOP_ARR = 126,
	BOT_ARR = 125,
	LEFT_ARR = 123,
	RIGHT_ARR = 124,
	MINUS = 78,
	PLUS = 69,
}					t_keycode;

typedef struct		s_coord
{
	int		x;
	int		y;
	int		z;
}					t_coord;

typedef struct		s_lcoord
{
	struct s_lcoord		*next;
	int					len;
	t_coord				*coord;
}					t_lcoord;

typedef struct		s_tab
{
	struct s_tab	*next;
	int				*tab;
	int				len;
}					t_tab;

typedef enum		e_color
{
	RED = 0x00FF4040,
	GREEN = 0x0040BB40,
	BLUE = 0x004040DD,
	ORANGE = 0x00FF6633,
	PURPLE = 0x00663366,
	YELLOW = 0x00FFFF66,
	BROWN = 0x00663315,
	PINK = 0x00FF69B4,
	OLIVE = 0x00808000,
	WHITE = 0x00FFFFFF,
}					t_color;

typedef struct		s_num
{
	int			pad;
	int			n_pad;
	t_color		color;
}					t_num;

typedef struct		s_e
{
	void			*mlx;
	void			*win;
	struct s_e		*copy;
	int				zoom;
	int				z_depth;
	int				parr;
	int				x_start;
	int				y_start;
	int				nb_line;
	int				max_len;
	t_color			color;
	t_tab			*tab;
	t_lcoord		*coord;
}					t_e;

t_tab				*ft_get_map(char *file, t_e *e);
void				draw_line(t_coord first, t_coord second,
		t_e *mlx, int color);
void				ft_my_hooks(t_e *e);
void				ft_proj(t_e *mlx);
void				ft_init_e(t_e *e, char *file);
void				ft_cal_coord(t_e *e);
int					color(int keycode);
void				ft_mem_fail(void);

#endif
