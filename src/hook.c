/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 13:01:24 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:14:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int		arrow_control(t_e *e, int keycode)
{
	if (keycode == TOP_ARR)
	{
		if (e->y_start > -WIN_H + 15)
			e->y_start -= 10;
		return (1);
	}
	else if (keycode == BOT_ARR)
	{
		if (e->y_start < WIN_H - 15)
			e->y_start += 10;
		return (1);
	}
	else if (keycode == LEFT_ARR)
	{
		if (e->x_start > -WIN_W + 15)
			e->x_start -= 10;
		return (1);
	}
	else if (keycode == RIGHT_ARR)
	{
		if (e->x_start < WIN_W - 15)
			e->x_start += 10;
		return (1);
	}
	return (0);
}

int				ft_hook_key(int keycode, void *param)
{
	t_e		*e;
	int		ret;

	e = (t_e*)param;
	if (keycode == ESC)
		exit(0);
	else if (arrow_control(e, keycode))
		;
	else if (keycode == PLUS)
		e->z_depth++;
	else if (keycode == MINUS)
		e->z_depth--;
	else if (keycode == SPACE)
		ft_memcpy(e, e->copy, sizeof(t_e));
	else if ((ret = color(keycode)))
		e->color = ret;
	ft_cal_coord(e);
	mlx_clear_window(e->mlx, e->win);
	ft_proj(e);
	return (0);
}

int				ft_hook_mouse(int button, int x, int y, void *param)
{
	t_e		*e;

	e = (t_e*)param;
	if (y || x)
		;
	if (button == W_TOP || button == W_LEFT)
	{
		if (e->zoom < WIN_H)
			e->zoom += 10;
	}
	else if (button == W_BOT || button == W_RIGHT)
	{
		if (e->zoom > 14)
			e->zoom -= 10;
	}
	ft_cal_coord(e);
	mlx_clear_window(e->mlx, e->win);
	ft_proj(e);
	return (0);
}

int				ft_hook_expose(void *param)
{
	t_e		*e;

	e = (t_e*)param;
	ft_cal_coord(e);
	mlx_clear_window(e->mlx, e->win);
	ft_proj(e);
	return (0);
}

void			ft_my_hooks(t_e *e)
{
	mlx_key_hook(e->win, ft_hook_key, e);
	mlx_mouse_hook(e->win, ft_hook_mouse, e);
	mlx_expose_hook(e->win, ft_hook_expose, e);
}
