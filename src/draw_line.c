/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:06:19 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:17:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_swap(int *a, int *b)
{
	int		c;

	c = *a;
	*a = *b;
	*b = c;
}

void		manage_swap_y(t_coord *f_point, t_coord *s_point)
{
	if (f_point->y > s_point->y)
	{
		ft_swap(&(f_point->x), &(s_point->x));
		ft_swap(&(f_point->y), &(s_point->y));
	}
}

double		init_draw(t_coord *f_point, t_coord *s_point)
{
	double		n;

	if (f_point->x > s_point->x)
	{
		ft_swap(&(f_point->x), &(s_point->x));
		ft_swap(&(f_point->y), &(s_point->y));
	}
	n = (double)(s_point->y - f_point->y) / (double)(s_point->x - f_point->x);
	return (n);
}

void		draw_line(t_coord f_point, t_coord s_point, t_e *mlx, int color)
{
	t_coord		draw;
	double		n;
	int			i;

	i = 0;
	n = init_draw(&f_point, &s_point);
	if (n <= 1 && n >= -1)
	{
		ft_memcpy(&draw, &f_point, sizeof(f_point));
		while (draw.x <= s_point.x)
		{
			mlx_pixel_put(mlx->mlx, mlx->win, draw.x++, draw.y, color);
			draw.y = f_point.y + (int)(++i * n);
		}
	}
	else
	{
		manage_swap_y(&f_point, &s_point);
		ft_memcpy(&draw, &f_point, sizeof(f_point));
		while (draw.y <= s_point.y)
		{
			mlx_pixel_put(mlx->mlx, mlx->win, draw.x, draw.y++, color);
			draw.x = f_point.x + (int)(++i / n);
		}
	}
}
