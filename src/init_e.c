/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_e.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:00:12 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:58:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_mlx_init(t_e *e)
{
	if (!(e->mlx = mlx_init()))
	{
		ft_putendl("Failed to initiate connexion to X-Server");
		exit(EXIT_FAILURE);
	}
	if (!(e->win = mlx_new_window(e->mlx, WIN_W, WIN_H, "Fil de Fer")))
	{
		ft_putendl("Failed to create window");
		exit(EXIT_FAILURE);
	}
}

void		ft_init_zoom(t_e *e)
{
	int		val1;
	int		val2;

	val1 = (WIN_H - WIN_H * 0.4) / e->nb_line;
	val2 = (WIN_W - WIN_W * 0.4) / e->max_len;
	e->zoom = (val1 > val2) ? val2 : val1;
}

void		ft_e_cpy(t_e *e)
{
	if (!(e->copy = malloc(sizeof(t_e))))
		ft_mem_fail();
	ft_memcpy(e->copy, e, sizeof(t_e));
}

void		ft_init_e(t_e *e, char *file)
{
	ft_bzero(e, sizeof(t_e));
	e->tab = ft_get_map(file, e);
	ft_mlx_init(e);
	e->z_depth = 1;
	e->color = COLOR;
	ft_init_zoom(e);
	e->parr = e->zoom / 3;
	e->x_start = WIN_H * 0.2;
	e->y_start = WIN_W * 0.2;
	ft_cal_coord(e);
	ft_e_cpy(e);
}
