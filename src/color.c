/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 14:21:24 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:06:45 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			color(int keycode)
{
	const t_num		c[10] = {
		{ 83, 18, RED },
		{ 84, 19, GREEN },
		{ 85, 20, BLUE },
		{ 86, 21, ORANGE },
		{ 87, 23, PURPLE },
		{ 88, 22, YELLOW },
		{ 89, 26, BROWN },
		{ 91, 28, PINK },
		{ 92, 25, OLIVE },
		{ 82, 29, WHITE },
	};
	int				i;

	i = 0;
	while (i < 10)
	{
		if (keycode == c[i].pad || keycode == c[i].n_pad)
			return (c[i].color);
		i++;
	}
	return (0);
}
