/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:04:51 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:45:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			ft_get_nb_int(char *str)
{
	int		ret;

	ret = 0;
	while (*str)
	{
		while (*str == ' ')
			str++;
		ret++;
		while (*str && *str != ' ')
			str++;
	}
	return (ret);
}

t_tab		*ft_tabnew(char *str, t_e *e)
{
	t_tab	*node;
	int		i;

	if (!(node = ft_memalloc(sizeof(t_tab))))
		ft_mem_fail();
	node->len = ft_get_nb_int(str);
	if (node->len > e->max_len)
		e->max_len = node->len;
	if (!(node->tab = malloc(sizeof(int) * node->len)))
		ft_mem_fail();
	i = 0;
	while (*str)
	{
		while (*str == ' ')
			str++;
		(node->tab)[i] = ft_atoi(str);
		while (*str && *str != ' ')
			str++;
		i++;
	}
	return (node);
}

void		ft_tabadd(t_tab **tab, char *str, t_e *e)
{
	t_tab		*tmp;

	if (!*tab)
		*tab = ft_tabnew(str, e);
	else
	{
		tmp = *tab;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = ft_tabnew(str, e);
	}
}

t_tab		*ft_get_map(char *file, t_e *e)
{
	int		fd;
	t_tab	*tab;
	char	*str;
	int		j;

	tab = NULL;
	j = 0;
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		perror("");
		exit(EXIT_FAILURE);
	}
	while (get_next_line(fd, &str) > 0)
	{
		if (*str)
			ft_tabadd(&tab, str, e);
		j++;
	}
	e->nb_line = j;
	return (tab);
}
