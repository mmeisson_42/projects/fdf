/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:00:17 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:41:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		main(int ac, char **av)
{
	t_e			e;

	if (ac != 2)
	{
		ft_putendl("Usage : ./fdf map_file");
		return (EXIT_FAILURE);
	}
	ft_init_e(&e, av[1]);
	ft_my_hooks(&e);
	ft_proj(&e);
	mlx_loop(e.mlx);
	return (EXIT_SUCCESS);
}
