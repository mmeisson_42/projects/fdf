/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:01:06 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:04:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_mem_fail(void)
{
	ft_putendl("Fatal Error: Could not alloc memory");
	exit(EXIT_FAILURE);
}
