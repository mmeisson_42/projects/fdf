/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cal_coord.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 16:08:28 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:22:43 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_lcoord	*ft_lcoordnew(int len)
{
	t_lcoord	*node;

	if (!(node = malloc(sizeof(t_lcoord))))
		ft_mem_fail();
	node->next = NULL;
	node->len = len;
	if (!(node->coord = ft_memalloc(sizeof(t_coord) * len)))
		ft_mem_fail();
	return (node);
}

void		ft_lcoordadd(t_lcoord **c, t_lcoord *node)
{
	t_lcoord	*tmp;

	if (!*c)
		*c = node;
	else
	{
		tmp = *c;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
}

void		ft_get_coord(t_lcoord *c, int j, t_tab *t, t_e *e)
{
	int		i;

	i = 0;
	while (i < t->len)
	{
		(c->coord)[i].x = (i * e->zoom - (t->tab)[i] * e->z_depth) +
			((e->nb_line - j) * e->parr) + e->x_start;
		(c->coord)[i].y = (j * e->zoom - (t->tab)[i] * e->z_depth) +
			e->y_start;
		i++;
	}
}

void		ft_cal_coord(t_e *e)
{
	static int	tem = 0;
	t_tab		*t;
	t_lcoord	*c;
	int			j;

	j = 0;
	t = e->tab;
	c = NULL;
	if (tem)
		c = e->coord;
	while (t)
	{
		if (!tem)
		{
			c = ft_lcoordnew(t->len);
			ft_lcoordadd(&(e->coord), c);
		}
		ft_get_coord(c, j++, t, e);
		if (tem)
			c = c->next;
		t = t->next;
	}
	tem = 1;
}
