/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_proj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 15:59:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/14 16:00:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_proj_ver(t_e *e, t_lcoord *c)
{
	int			i;

	i = 0;
	if (c->next)
	{
		while (i < c->len && i < c->next->len)
		{
			draw_line((c->coord)[i], (c->next->coord)[i], e, e->color);
			i++;
		}
	}
}

static void		ft_proj_hor(t_e *e, t_lcoord *c)
{
	int			i;

	i = c->len;
	while (--i > 0)
		draw_line((c->coord)[i - 1], (c->coord)[i], e, e->color);
}

void			ft_proj(t_e *e)
{
	t_lcoord	*c;

	c = e->coord;
	while (c)
	{
		ft_proj_hor(e, c);
		ft_proj_ver(e, c);
		c = c->next;
	}
}
